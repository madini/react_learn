import {ADD_TO_CART,DELETE_FROM_CART,UPDATE_CART} from "../../actions/cart/cart-actions"
import {CartActions} from '../../actions/cart/type'
import { CartState } from "./type";
const initialState:CartState ={
    cart: [
        {
          product: 'bread 700g',
          quantity: 2,
          unitCost: 90,
        },
        {
          product: 'milk 500ml',
          quantity: 1,
          unitCost: 47,
        },
        {
          product:'mookey toy',
          quantity:100,
          unitCost:200
        }
      ]
}
export const cartReducer = function(state = initialState ,action:CartActions ){
    const {type,payload} = action;
    switch(type){
        case ADD_TO_CART:{
            return{
                ...state,
                cart:[...state.cart , action.payload]
            }
        }
        case DELETE_FROM_CART:{
          return{
            ...state,
            cart:state.cart.filter(item=>item.product!==payload.product)
          }
        }
        case UPDATE_CART:{
          return{
            ...state,
            cart:state.cart.map(item=>item.product===payload.product? payload : item )
          }
        }
        default:
          return state;
    }
 
}
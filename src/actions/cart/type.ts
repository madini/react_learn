import { type } from "os";
import { Map } from "typescript";
import {ADD_TO_CART,UPDATE_CART,DELETE_FROM_CART} from "./cart-actions"
export interface Product{
  product: string,
  quantity: number,
  unitCost: number,
}

export interface AddToCartAction {
    type:typeof ADD_TO_CART,
    payload:Product
}

export interface DeleteFromCartAction {
  type:typeof DELETE_FROM_CART,
  payload:{product:Product['product']}
}

export interface UpdateCartAction {
  type: typeof UPDATE_CART,
  payload:Product
}

export type CartActions = AddToCartAction|DeleteFromCartAction|UpdateCartAction

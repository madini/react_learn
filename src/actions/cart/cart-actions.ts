import { Product } from "./type";

export const ADD_TO_CART  = 'ADD_TO_CART';
export const DELETE_FROM_CART  = 'DELETE_FROM_CART'
export const UPDATE_CART  = "UPDATE_CART"
export const addToCart = (product:Product)=>{
    return{
        type:ADD_TO_CART,
        payload:{...product}
    }
}
export const deleteFromCart =(product:string)=>{
    return {
        type:DELETE_FROM_CART,
        payload:{
            product
        }
    }
}
export const updateCart =(product:Product)=>{
    return {
        type:UPDATE_CART,
        payload:{
            ...product
        }
    }
}
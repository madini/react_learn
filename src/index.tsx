import React, { ReactChild } from 'react';
import ReactDOM from 'react-dom';
import store from "./store";
import { addToCart,deleteFromCart ,updateCart}  from './actions/cart/cart-actions';
import { Provider } from 'react-redux';
const App:React.FC = ()=> <h1>Redux Shopping Cart</h1>;

console.log("initial state: ", store.getState());

let unsubscribe = store.subscribe(()=>{
    console.log(store.getState())
})
store.dispatch(addToCart({product:'Coffee 500gm', quantity:1,unitCost: 250}));
store.dispatch(addToCart({product:'Flour 1kg',quantity: 2,unitCost: 110}));
store.dispatch(deleteFromCart('Coffee 500gm'))
store.dispatch(updateCart({product:'Flour 1kg',quantity: 100,unitCost: 300}))
unsubscribe();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
  document.getElementById('root')
);